import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.patheffects as pe

plt.style.use('ggplot')

def plot_exec_time_over_time(sizes, exec_timesA, exec_timesB, exec_timesC, exec_timesD, exec_timesE, option, file_path):
    f1 = plt.figure()
    plt.plot(sizes, exec_timesA, label="Insertion Sort", color='C0')
    plt.plot(sizes, exec_timesB, label="Selection Sort", color='C1')
    plt.plot(sizes, exec_timesC, label="Mergesort", color='C2')
    plt.plot(sizes, exec_timesD, label="Heapsort", color='C3')
    plt.plot(sizes, exec_timesE, label="Quicksort", color='C4')
    plt.title("Tempo necessário para ordenar\n com " + str(option) + ".",
              fontsize=10, fontweight='bold')
    plt.xlabel("n", fontsize=10)
    plt.ylabel("Tempo de execução (s)", fontsize=10)
    plt.legend(fontsize=9)
    f1.savefig(str(file_path), format='pdf', bbox_inches='tight')
    plt.cla()

def plot_exec_time_over_timeFastAlgorithms(sizes, exec_timesC, exec_timesD, exec_timesE, option, file_path):
    f1 = plt.figure()
    plt.plot(sizes, exec_timesC, label="Mergesort", color='C2')
    plt.plot(sizes, exec_timesD, label="Heapsort", color='C3')
    plt.plot(sizes, exec_timesE, label="Quicksort", color='C4')
    plt.title("Tempo necessário para ordenar\n com " + str(option) + ".",
              fontsize=10, fontweight='bold')
    plt.xlabel("n", fontsize=10)
    plt.ylabel("Tempo de execução (s)", fontsize=10)
    plt.legend(fontsize=9)
    f1.savefig(str(file_path), format='pdf', bbox_inches='tight')
    plt.cla()

def plot_exec_time_over_timeSlowAlgorithms(sizes, exec_timesA, exec_timesB, option, file_path):
    f1 = plt.figure()
    plt.plot(sizes, exec_timesA, label="Insertion Sort", color='C0')
    plt.plot(sizes, exec_timesB, label="Selection Sort", color='C1')
    plt.title("Tempo necessário para ordenar\n com " + str(option) + ".",
              fontsize=10, fontweight='bold')
    plt.xlabel("n", fontsize=10)
    plt.ylabel("Tempo de execução (s)", fontsize=10)
    plt.legend(fontsize=9)
    f1.savefig(str(file_path), format='pdf', bbox_inches='tight')
    plt.cla()