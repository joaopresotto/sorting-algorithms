import random

# ************** #
# Insertion Sort #
# ************** #
def insertion_sort(A):
    for i in range(1, len(A)):
        key = A[i]
        k = i
        while k > 0 and key < A[k - 1]:
            A[k] = A[k - 1]
            k -= 1
        A[k] = key

    return A

# ************** #
# Selection Sort #
# ************** #
def selection_sort(A):
    for i in range(len(A)):
        min = i
        for j in range(i+1, len(A)):
            if A[min] > A[j]:
                min = j

        A[i], A[min] = A[min], A[i]

    return A

# ********** #
# Merge Sort #
# ********** #
def merge_sort(A):
    if len(A) <= 1:
        return A
    else:
        left, right = split(A)
        return merge(merge_sort(left), merge_sort(right))


def split(A):
    input_list_len = len(A)
    midpoint = input_list_len // 2
    return A[:midpoint], A[midpoint:]


def merge(list_left, list_right):
    # Special case: one or both of lists are empty
    if len(list_left) == 0:
        return list_right
    elif len(list_right) == 0:
        return list_left

    # General case
    index_left = index_right = 0
    list_merged = []  # list to build and return
    list_len_target = len(list_left) + len(list_right)
    while len(list_merged) < list_len_target:
        if list_left[index_left] <= list_right[index_right]:
            # Value on the left list is smaller (or equal so it should be selected)
            list_merged.append(list_left[index_left])
            index_left += 1
        else:
            # Right value bigger
            list_merged.append(list_right[index_right])
            index_right += 1

        # If we are at the end of one of the lists we can take a shortcut
        if index_right == len(list_right):
            # Reached the end of right
            # Append the remainder of left and break
            list_merged += list_left[index_left:]
            break
        elif index_left == len(list_left):
            # Reached the end of left
            # Append the remainder of right and break
            list_merged += list_right[index_right:]
            break

    return list_merged

# ********* #
# Heapsort #
# ********* #
def heap_sort(A):
    n = len(A)

    # Build a maxheap.
    for i in range(n, -1, -1):
        max_heapify(A, n, i)

    # One by one extract elements
    for i in range(n-1, 0, -1):
        A[i], A[0] = A[0], A[i]   # swap
        max_heapify(A, i, 0)

    return A

# To heapify subtree rooted at index i.
# n is size of heap
def max_heapify(A, n, i):
    largest = i  # Initialize largest as root
    l = 2 * i + 1     # left = 2*i + 1
    r = 2 * i + 2     # right = 2*i + 2

    # See if left child of root exists and is
    # greater than root
    if l < n and A[i] < A[l]:
        largest = l

    # See if right child of root exists and is
    # greater than root
    if r < n and A[largest] < A[r]:
        largest = r

    # Change root, if needed
    if largest != i:
        A[i], A[largest] = A[largest], A[i]  # swap

        # Heapify the root.
        max_heapify(A, n, largest)

    return A

# ********* #
# Quicksort #
# ********* #
def quick_sort(A, low, high):
    if low < high:
        # p is partitioning index, A[p] is now
        # at right place
        p = random_partition(A, low, high)

        # Separately sort elements before
        # partition and after partition
        quick_sort(A, low, p-1)
        quick_sort(A, p+1, high)

    return A

# This function takes last element as pivot, places
# the pivot element at its correct position in sorted
# array, and places all smaller (smaller than pivot)
# to left of pivot and all greater elements to right
# of pivot
def partition(A, low, high):
    i = (low-1)         # index of smaller element
    pivot = A[high]     # pivot

    for j in range(low, high):

        # If current element is smaller than or
        # equal to pivot
        if A[j] <= pivot:

            # increment index of smaller element
            i = i+1
            A[i], A[j] = A[j], A[i]

    A[i+1], A[high] = A[high], A[i+1]
    return i+1

def random_partition(A, low, high):
    i = random.randint(low,high)
    A[i], A[high] = A[high], A[i]
    return partition(A, low, high)