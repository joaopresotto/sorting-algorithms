import sorting_algorithms as sort
import sys
import copy
import plots as plt
import numpy as np
import statistics
import random
import time
import scipy

def run_experiments(option):
    execution_timesInsertion, execution_timesSelection, execution_timesMerge, execution_timesHeap, execution_timesQuick = [],[],[],[],[]
    stddev_timesInsertion, stddev_timesSelection, stddev_timesMerge, stddev_timesHeap, stddev_timesQuick = [],[],[],[],[]
    sample_size = []
    n_executions = 5
    for n in range(10,20): 
        size = 2**n
        sample_size.append(size)

        vet_sorted = [i for i in range(size)]
        vet_sorted_inverted = [size-i for i in range(size)]
        vet_random = copy.deepcopy(vet_sorted)
        random.shuffle(vet_random)

        for j in range(5):
            aux = []
            time_list = []
            if j == 0:
            # **
            # Insertion Sort 
            # **
                for i in range(n_executions):
                    if option == 0:
                        aux = copy.deepcopy(vet_random)
                    elif option == 1:
                        aux = copy.deepcopy(vet_sorted)
                    else: # option == 2
                        aux = copy.deepcopy(vet_sorted_inverted)
                    start = time.time()
                    sort.insertion_sort(aux)
                    end = time.time()
                    time_list.append(end-start)

                execution_timesInsertion.append(statistics.mean(time_list))
                stddev_timesInsertion.append(statistics.stdev(time_list))
            elif j == 1:    
            # **
            # Selection Sort
            # **
                for i in range(n_executions):
                    if option == 0:
                        aux = copy.deepcopy(vet_random)
                    elif option == 1:
                        aux = copy.deepcopy(vet_sorted)
                    else: # option == 2
                        aux = copy.deepcopy(vet_sorted_inverted)
                    start = time.time()
                    sort.selection_sort(aux)
                    end = time.time()
                    time_list.append(end-start)
                
                execution_timesSelection.append(statistics.mean(time_list))
                stddev_timesSelection.append(statistics.stdev(time_list))
            elif j == 2:
            # **
            # Merge Sort
            # **
                for i in range(n_executions):
                    if option == 0:
                        aux = copy.deepcopy(vet_random)
                    elif option == 1:
                        aux = copy.deepcopy(vet_sorted)
                    else: # option == 2
                        aux = copy.deepcopy(vet_sorted_inverted)
                    start = time.time()
                    sort.merge_sort(aux)
                    end = time.time()
                    time_list.append(end-start)
                    
                execution_timesMerge.append(statistics.mean(time_list))
                stddev_timesMerge.append(statistics.stdev(time_list))
            elif j == 3:
            # **
            # Heap Sort
            # **
                for i in range(n_executions):
                    if option == 0:
                        aux = copy.deepcopy(vet_random)
                    elif option == 1:
                        aux = copy.deepcopy(vet_sorted)
                    else: # option == 2
                        aux = copy.deepcopy(vet_sorted_inverted)
                    start = time.time()
                    sort.heap_sort(aux)
                    end = time.time()
                    time_list.append(end-start)
                    
                execution_timesHeap.append(statistics.mean(time_list))
                stddev_timesHeap.append(statistics.stdev(time_list))
            else: # j == 4
            # **
            # Quick Sort
            # **
                for i in range(n_executions):
                    if option == 0:
                        aux = copy.deepcopy(vet_random)
                    elif option == 1:
                        aux = copy.deepcopy(vet_sorted)
                    else: # option == 2
                        aux = copy.deepcopy(vet_sorted_inverted)
                    #if option != 0:
                    #    sys.setrecursionlimit(max(sys.getrecursionlimit(), len(aux)+9999999))
                    start = time.time()
                    sort.quick_sort(aux,0,len(aux)-1)
                    end = time.time()
                    time_list.append(end-start)
                    
                execution_timesQuick.append(statistics.mean(time_list))
                stddev_timesQuick.append(statistics.stdev(time_list)) 

    return sample_size, execution_timesInsertion, execution_timesSelection, execution_timesMerge, execution_timesHeap, execution_timesQuick
        

## MAIN
# Options:
# 0 -> random array
# 1 -> ordered array
# 2 -> inverted ordered array
sample_size, execution_timesInsertion, execution_timesSelection, execution_timesMerge, execution_timesHeap, execution_timesQuick = run_experiments(0)
plt.plot_exec_time_over_time(sample_size,execution_timesInsertion,execution_timesSelection,execution_timesMerge,execution_timesHeap,execution_timesQuick,"entrada aleatória","outRandom.pdf")
plt.plot_exec_time_over_timeFastAlgorithms(sample_size,execution_timesMerge,execution_timesHeap,execution_timesQuick,"entrada aleatória","outRandomOnly3.pdf")
plt.plot_exec_time_over_timeSlowAlgorithms(sample_size,execution_timesInsertion, execution_timesSelection,"entrada aleatória","outRandomOnly2.pdf")

print("Experimento 1")
print("sample_size = ", sample_size)
print("Insertion_Sort =", execution_timesInsertion)
print("Selection_Sort =", execution_timesSelection)
print("Merge_Sort =", execution_timesMerge)
print("Heap_Sort =", execution_timesHeap)
print("Quick_Sort =", execution_timesQuick)

sample_size, execution_timesInsertion, execution_timesSelection, execution_timesMerge, execution_timesHeap, execution_timesQuick = run_experiments(1)
plt.plot_exec_time_over_time(sample_size,execution_timesInsertion,execution_timesSelection,execution_timesMerge,execution_timesHeap,execution_timesQuick,"entrada ordenada","outOrdered.pdf")
plt.plot_exec_time_over_timeFastAlgorithms(sample_size,execution_timesMerge,execution_timesHeap,execution_timesQuick,"entrada ordenada","outOrderedOnly2.pdf")
plt.plot_exec_time_over_timeSlowAlgorithms(sample_size,execution_timesInsertion, execution_timesSelection,"entrada ordenada","outOrderedOnly2.pdf")

print("Experimento 2")
print("sample_size = ", sample_size)
print("Insertion_Sort =", execution_timesInsertion)
print("Selection_Sort =", execution_timesSelection)
print("Merge_Sort =", execution_timesMerge)
print("Heap_Sort =", execution_timesHeap)
print("Quick_Sort =", execution_timesQuick)

sample_size, execution_timesInsertion, execution_timesSelection, execution_timesMerge, execution_timesHeap, execution_timesQuick = run_experiments(2)
plt.plot_exec_time_over_time(sample_size,execution_timesInsertion,execution_timesSelection,execution_timesMerge,execution_timesHeap,execution_timesQuick,"entrada inversamente ordenada","outOrderedInverted.pdf")
plt.plot_exec_time_over_timeFastAlgorithms(sample_size,execution_timesMerge,execution_timesHeap,execution_timesQuick,"entrada inversamente ordenada","outInvertedOnly3.pdf")
plt.plot_exec_time_over_timeSlowAlgorithms(sample_size,execution_timesInsertion, execution_timesSelection,"entrada inversamente ordenada","outOrderedInvertedOnly2.pdf")

print("Experimento 3")
print("sample_size = ", sample_size)
print("Insertion_Sort =", execution_timesInsertion)
print("Selection_Sort =", execution_timesSelection)
print("Merge_Sort =", execution_timesMerge)
print("Heap_Sort =", execution_timesHeap)
print("Quick_Sort =", execution_timesQuick)